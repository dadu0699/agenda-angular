import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

//Rutas
import { app_routing } from './app.routes';

//Servicios
import { AuthGuardService } from './services/auth-guard.service';
import { UsuarioService } from './services/usuario.service';
import { CategoriaService } from './services/categoria.service';
import { ContactoService } from './services/contacto.service';
import { TareaService } from './services/tarea.service';
import { HistorialService } from './services/historial.service';

//Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NavbarComponent } from './components/dashboard/navbar/navbar.component';
import { BodyComponent } from './components/dashboard/body/body.component';
import { CategoriasComponent } from './components/dashboard/categorias/categorias.component';
import { CategoriasFormAgregarComponent } from './components/dashboard/categorias/categorias-form-agregar.component';
import { ContactosComponent } from './components/dashboard/contactos/contactos.component';
import { ContactosFormAgregarComponent } from './components/dashboard/contactos/contactos-form-agregar.component';
import { ContactosFormEditarComponent } from './components/dashboard/contactos/contactos-form-editar.component';
import { TareasComponent } from './components/dashboard/tareas/tareas.component';
import { TareasFormAgregarComponent } from './components/dashboard/tareas/tareas-form-agregar.component';
import { TareasFormEditarComponent } from './components/dashboard/tareas/tareas-form-editar.component';
import { HistorialesComponent } from './components/dashboard/historiales/historiales.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    NavbarComponent,
    BodyComponent,
    CategoriasComponent,
    CategoriasFormAgregarComponent,
    ContactosComponent,
    ContactosFormAgregarComponent,
    ContactosFormEditarComponent,
    TareasComponent,
    TareasFormAgregarComponent,
    TareasFormEditarComponent,
    HistorialesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    app_routing
  ],
  providers: [
    AuthGuardService,
    UsuarioService,
    CategoriaService,
    ContactoService,
    TareaService,
    HistorialService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
