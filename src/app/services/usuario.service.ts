import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsuarioService {
  uriUsuario = "http://localhost:3000/api/v1/usuario/";
  usuarios:any[];
  constructor(private http:Http, private router: Router) {}

  public autenticar(usuario: any) {
    let uriUsuarioAutenticar:string = "http://localhost:3000/auth/";

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    //headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzdWFyaW8iOjEsIm5pY2siOiJhZG1pbiIsImNvbnRyYXNlbmEiOiJhZG1pbiIsImlhdCI6MTQ5OTk2Nzk0MywiZXhwIjoxNDk5OTY4MDAzfQ.yONAEXitFluD0lLBPozptOU1-8RUg6aQlG2OsicNFbo');

    let options = new RequestOptions({headers: headers});
    let data = JSON.stringify(usuario);

    this.http.post(uriUsuarioAutenticar, data, options).subscribe(res => {
      //console.log(res.json());
      let token = res.json().token;
      if(token){
        console.log("Si existe el token");
        localStorage.setItem('token', token);
        localStorage.setItem('usuario', JSON.stringify(res.json().usuario));
        this.router.navigate(['/dashboard']);
      } else {
        console.log("No existe token");
        return false;
      }
    }, error => {
      console.log(error.text());
    });
  }

  public verificarUsuario():boolean {
    if (localStorage.getItem('token')){
      return true;
    }
    return false;
  }

  public resgistrar(usuario: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({headers: headers});
    let data = JSON.stringify(usuario);

    this.http.post(this.uriUsuario, data, options).subscribe(res => {
      this.autenticar(usuario);
    }, error => {
      console.log(error.text());
    });
  }
}
