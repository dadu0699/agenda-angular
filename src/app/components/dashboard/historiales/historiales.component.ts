import { Component, OnInit } from '@angular/core';
import { HistorialService } from '../../../services/historial.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historiales.component.html',
})
export class HistorialesComponent implements OnInit {
  historiales:any[] = [];

  constructor(
    private historialService:HistorialService
  ) {  }

  public inicializar() {
    this.historialService.getHistoriales().subscribe(data => {
      this.historiales = data;
    });
  }

  ngOnInit() {
    this.inicializar();
  }
}
