import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriaService } from '../../../services/categoria.service';
import { CategoriasComponent } from './categorias.component';

@Component({
  selector: 'app-formAgregar-categoria',
  templateUrl: './categorias-form-agregar.component.html',
})
export class CategoriasFormAgregarComponent implements OnInit {
  notificacion:any = {
    estado: false,
    mensaje: ""
  }
  formularioAddCategoria:FormGroup;

  constructor(
    private categoriaService:CategoriaService,
    private categoriasComponent:CategoriasComponent
  ) {  }

  ngOnInit() {
    this.formularioAddCategoria = new FormGroup({
      'nombreCategoria': new FormControl('', Validators.required)
    });
  }

  public addCategoria() {
    this.categoriaService.nuevaCategoria(this.formularioAddCategoria.value)
    .subscribe(res => {
      if(res.estado) {
        this.notificacion.mensaje = res.mensaje;
        this.notificacion.estado = res.estado;
        this.formularioAddCategoria.reset();
        this.categoriasComponent.inicializar();
      }
    });
  }
}
