import { Routes } from '@angular/router';
import { BodyComponent } from './body/body.component';
import { CategoriasComponent } from './categorias/categorias.component';
import { ContactosComponent } from './contactos/contactos.component';
import { ContactosFormEditarComponent } from './contactos/contactos-form-editar.component';
import { TareasComponent } from './tareas/tareas.component';
import { TareasFormEditarComponent } from './tareas/tareas-form-editar.component';
import { HistorialesComponent } from './historiales/historiales.component';

export const dashboard_routes: Routes = [
  { path: 'categorias', component: CategoriasComponent },
  { path: 'contactos', component: ContactosComponent },
  { path: 'contacto/:idContacto', component: ContactosFormEditarComponent },
  { path: 'tareas', component: TareasComponent },
  { path: 'tarea/:idTarea', component: TareasFormEditarComponent },
  { path: 'historial', component: HistorialesComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'contactos' }
];
