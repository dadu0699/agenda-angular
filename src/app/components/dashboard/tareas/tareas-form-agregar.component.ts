import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TareaService } from '../../../services/tarea.service';
import { TareasComponent } from './tareas.component';

@Component({
  selector: 'app-formAgregar-tarea',
  templateUrl: './tareas-form-agregar.component.html',
})
export class TareasFormAgregarComponent implements OnInit {
  estados:any[] = [];
  notificacion:any = {
    estado: false,
    mensaje: ""
  }
  formularioAddTarea:FormGroup;

  constructor(
    private tareaService:TareaService,
    private tareasComponent:TareasComponent
  ) {  }

  ngOnInit() {
    this.tareaService.getEstados().subscribe(data => {
      this.estados = data;
    });

    this.formularioAddTarea = new FormGroup({
      'titulo': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'idEstado': new FormControl('', Validators.required),
      'fechaFin': new FormControl('', Validators.required)
    });
  }

  public addTarea() {
    this.tareaService.nuevaTarea(this.formularioAddTarea.value)
    .subscribe(res => {
      if(res.estado) {
        this.notificacion.mensaje = res.mensaje;
        this.notificacion.estado = res.estado;
        this.formularioAddTarea.reset();
        this.tareasComponent.inicializar();
      }
    });
  }
}
