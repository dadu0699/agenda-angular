import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TareaService } from '../../../services/tarea.service';

@Component({
  selector: 'app-formEditar-tarea',
  templateUrl: './tareas-form-editar.component.html',
})
export class TareasFormEditarComponent implements OnInit {
  formularioTarea:FormGroup;
   uri:string;
   tarea:any;

   constructor(
     private tareaService:TareaService,
     private router:Router,
     private activatedRoute:ActivatedRoute
   ) {

     this.activatedRoute.params.subscribe(params => {
       this.uri = params["idTarea"];
       this.tareaService.getTarea(params["idTarea"])
       .subscribe(tarea => {
         this.tarea = tarea;
         this.formularioTarea = new FormGroup({
           'titulo': new FormControl(this.tarea.titulo, Validators.required),
           'descripcion': new FormControl(this.tarea.descripcion, Validators.required),
           'idEstado': new FormControl(this.tarea.idEstado, Validators.required),
           'fechaFin': new FormControl(this.tarea.fin, Validators.required)
         });
       });
     });
   }

   ngOnInit() {
   }

   public guardarCambios() {
     console.log("Modificacion de tarea");
     this.tareaService.editarTarea(this.formularioTarea.value, this.uri)
     .subscribe(res => {
       console.log(res);
     });
   }
}
