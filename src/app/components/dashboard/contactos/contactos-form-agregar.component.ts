import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactoService } from '../../../services/contacto.service';
import { ContactosComponent } from './contactos.component';

@Component({
  selector: 'app-formAgregar-contacto',
  templateUrl: './contactos-form-agregar.component.html',
})
export class ContactosFormAgregarComponent implements OnInit {
  categorias:any[] = [];
  notificacion:any = {
    estado: false,
    mensaje: ""
  }
  formularioAddContacto:FormGroup;
  validaciones = [
    Validators.required, Validators.email
  ];

  constructor(
    private contactoService:ContactoService,
    private contactosComponent:ContactosComponent
  ) {  }

  ngOnInit() {
    this.contactoService.getCategorias().subscribe(data => {
      this.categorias = data;
    });

    this.formularioAddContacto = new FormGroup({
      'idCategoria': new FormControl('', Validators.required),
      'nombre': new FormControl('', Validators.required),
      'apellido': new FormControl('', Validators.required),
      'direccion': new FormControl('', Validators.required),
      'telefono': new FormControl('', Validators.required),
      'correo': new FormControl('', this.validaciones)
    });
  }

  public addContacto() {
    //console.log("Nuevo Contacto");
    //console.log(this.formularioAddContacto.value);
    this.contactoService.nuevoContacto(this.formularioAddContacto.value)
    .subscribe(res => {
      if(res.estado) {
        this.notificacion.mensaje = res.mensaje;
        this.notificacion.estado = res.estado;
        this.formularioAddContacto.reset();
        this.contactosComponent.inicializar();
      }
    });
  }
}
