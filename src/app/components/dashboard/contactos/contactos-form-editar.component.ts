import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactoService } from '../../../services/contacto.service';

@Component({
  selector: 'app-formEditar-contacto',
  templateUrl: './contactos-form-editar.component.html',
})
export class ContactosFormEditarComponent implements OnInit {
  formularioContacto:FormGroup;
   uri:string;
   contacto:any;

   constructor(
     private contactoService:ContactoService,
     private router:Router,
     private activatedRoute:ActivatedRoute
   ) {
     
     this.activatedRoute.params.subscribe(params => {
       this.uri = params["idContacto"];
       this.contactoService.getContacto(params["idContacto"])
       .subscribe(contacto => {
         this.contacto = contacto;
         this.formularioContacto = new FormGroup({
           'nombre': new FormControl(this.contacto.nombre, Validators.required),
           'apellido': new FormControl(this.contacto.apellido, Validators.required),
           'direccion': new FormControl(this.contacto.direccion, Validators.required),
           'telefono': new FormControl(this.contacto.telefono, Validators.required),
           'correo': new FormControl(this.contacto.correo, Validators.required),
           'idCategoria': new FormControl(this.contacto.idCategoria,  Validators.required),
         });
       });
     });
   }

   ngOnInit() {
   }

   public guardarCambios() {
     console.log("Modificacion de contacto");
     this.contactoService.editarContacto(this.formularioContacto.value, this.uri)
     .subscribe(res => {
       console.log(res);
     });
   }
}
