import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles: []
})
export class SignupComponent implements OnInit {
  formularioSignUp:FormGroup;

  constructor(private usuarioService:UsuarioService) {  }

  ngOnInit() {
    this.formularioSignUp = new FormGroup({
      'nick': new FormControl('', Validators.required),
      'contrasena': new FormControl('', Validators.required)
    });
  }

  public registrar() {
    //console.log(this.formularioSignUp.value);
    this.usuarioService.resgistrar(this.formularioSignUp.value);
  }
}
